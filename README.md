# KatelynOS

A very simple  32-bit hobby OS written in C (and a little assembly)
The kernel is a very very basic monolithic kernel and only exposes a few functions so it isn't really a kernel.
Most of the code here is derived from tutorials on [OSDev](https://wiki.osdev.org/Expanded_Main_Page).

## Important files and directories

- boot.asm - This is a bootloader stub. It sets up a multiboot environment so that GRUB is able to boot it. It then loads the kernel.
- kernel.c - This is the kernel. It is executed by the bootloader stub and has a function (kernel_main) that does everything.
- linker.ld - This is a linker script. It tells the compiler where certain sections of the code should go in memory.
- images/  - This directory contains iso images produced by the build script.
- bld/ - This directory contains compiled files produced by the build script.
- lib/ - This directory contains header files that can be used by the kernel.
- scripts/ - This directory contains building and testing scripts for convinience.
- iso/ - This directory stores the outputted kernel ELF and a GRUB configuration which is used to make an ISO image.

## Useful libraries

The lib directory stores header files that can be used as libraries by the kernel to expose certain functions.

- console.h (currently useless) - Will eventually implement a set of routines for a shell to use.
- debug.h - Contains some code for testing functions and a function (debug_writemsg) for debugging to a serial port and the inbuilt terminal.
- ioport.h - A wrapper for the assembly functions inb and outb which are used to control certain hardware ports.
- keyboard.h (currently useless) - A very minimal PS/2 keyboard driver.
- serial.h - A serial interface. This can be used with QEMU's -serial option as a handy debugging interface.
- string.h - Contains some implmentations of C string functions.
- terminal.h - A very useful library that contains functions pertaining to VGA graphics and a terminal framebuffer.

## Todo

I still have lots to do and learn about OS development. My main goals at the moment are to

- Develop and understand global descriptor tables (GDTs) and interrupt descriptor tables (IDTs).
- Implement interrupt request (IRQ) support so that the OS can interface with hardware.
- Flesh out the keyboard driver so that it can do more than just read scan codes.
- Implement proper exception support and kernel panicking.
- Seriously refactor and document spaghetti code.
- Make header files correct.

A more detailed list of TODOs can be found in todo.txt.

## Compiling 

To compile KatelynOS, you will need:

- GCC cross compiler (You can find a tutorial for this [here.](https://wiki.osdev.org/GCC_Cross-Compiler))
- NASM
- ld (The GNU linker)
- GRUB2 and grub-mkrescue
- xorriso

Simply execute scripts/build.sh to compile the kernel and make a bootable ISO.

## Running 

To run KatelynOS, you will need QEMU installed. 
Simply execute scripts/test.sh to run KatelynOS in QEMU with the recommended parameters.

I have not yet tested KatelynOS on real hardware, but theoretically you should be able to use DD to put the ISO in images/ onto a DVD and boot it.

