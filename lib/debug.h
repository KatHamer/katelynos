/*
Debug
Debugging and testing tools for KatelynOS
*/


#ifndef _DEBUGH_
#define _DEBUGH_

#include "terminal.h"
#include "serial.h"

int serial = 1;
int terminal = 1;

void test_colors() {
    serial_writestring("Testing colours...");
    for (size_t i = 1; i < 15; i++) {
        terminal_setcolor(i);
        terminal_writestring("!");
    }
}

void debug_printmsg(char* label, char* message) {
    if (terminal) {
    terminal_setcolor(VGA_COLOR_WHITE);
    terminal_writestring(" [");
    terminal_setcolor(VGA_COLOR_LIGHT_BLUE);
    terminal_writestring(label);
    terminal_setcolor(VGA_COLOR_WHITE);
    terminal_writestring("] ");
    terminal_writestring(message);
    terminal_writestring("\n");

    }

    if (serial) {
    serial_writestring(" [");
    serial_writestring(label);
    serial_writestring("] ");
    serial_writestring(message);
    serial_writestring("\n");
    }

}

#endif
