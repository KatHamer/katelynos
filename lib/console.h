/*
Console
KatelynOS console
*/


#ifndef _CONSOLEH_
#define _CONSOLEH_

#include "terminal.h"
#include "keyboard.h"

void console_prompt() {
    while(1) {
       terminal_setcolor(VGA_COLOR_WHITE);
       terminal_writestring("\n> \n");
       waitforkey();
    }
}

#endif
