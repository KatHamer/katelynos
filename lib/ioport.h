/*
IOPort.c
IOPort library for KatelynOS
By Kat Hamer
*/


#ifndef _IOPORTH_
#define _IOPORTH_

#define PORT 0x3f8   /* COM1 */

inline unsigned char inb(unsigned int port)
{
   unsigned char ret;
   asm volatile ("inb %%dx,%%al":"=a" (ret):"d" (port));
   return ret;
}


inline void outb(unsigned int port,unsigned char value)
{
   asm volatile ("outb %%al,%%dx": :"d" (port), "a" (value));
}

#endif
