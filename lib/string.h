/*
String.h
String library for KatelynOS
By Kat Hamer
*/

#ifndef _STRINGH_
#define _STRINGH_

#include <stdint.h>

size_t strlen(const char* str)
{
    size_t len = 0;
    while (str[len])
        len++;
    return len;
}

//char* strcat(char* first, char* second) {
//    size_t first_len = strlen(first);
//   size_t second_len = strlen(second);
//    char result[first_len + second_len];
//    for (size_t i = 0; i < first_len; i++) {
//        result[i] = first[i];
//    }
//    for (size_t i = first_len; i < first_len + second_len; i++) {
//        result[i] = second[i];
//    }
//    return result;
// }

#endif
