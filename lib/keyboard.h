/*
Keyboard
PS/2 keyboard driver for KatelynOS
*/

// Protect against the compiler reloading the same file
#ifndef _KEYBOARDH_
#define _KEYBOARDH_

#include "ioport.h"  // Communicate over serial
#include "debug.h"   // Write debug messages

// Response bytes - The keyboard will respond with one of these when a command is sent
int ERROR = 0x00;
int TEST_PASSED = 0xAA;
int TEST_FAILED = 0xFC;
int RESEND = 0xFE;
int ECHO = 0xEE;
int ACK = 0xFA;

// Commands
int DISABLE_SCANNING = 0xF5;
int IDENTIFY = 0xF2;

// Keyboard ports
int status_port = 0x64;  // A 7 bit register containing the status of the keyboard, also acts as a command register if written to
int data_port = 0x60;  // Contains data read from the keyboard

// Send a command to the keyboard
void command(int command) {
    debug_log("KEYBOARD", strcat("Sending command to keyboard...", itoa(command, 10)), VGA_COLOR_CYAN);
    outb(status_port, command);
}

// Read a response from the keyboard
size_t read(int bytes) {
    int bytes_read = 0;
	int buffer[bytes];
	while (bytes_read < bytes) {
        buffer[bytes_read] = inb(data_port);
	}
	return buffer;
}

// Ask the keyboard to identify itself
size_t identify() {
    uint16_t identity;
    command(DISABLE_SCANNING);
	while(read(4) != ACK);
	command(IDENTIFY);
    while(read(4) != ACK);
	identity = read(8);
	return identity;
}

#endif

