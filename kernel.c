/*
KatelynOS Kernel
By Kat Hamer
*/


#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "lib/string.h"
#include "lib/terminal.h"
#include "lib/serial.h"
#include "lib/debug.h"
#include "lib/console.h"

// Kernel main function
void kernel_main()
{

    /* Initialize serial and terminal interfaces */
    serial_initialize();
    terminal_initialize();
    debug_printmsg("target", "Booted!");
    debug_printmsg("target", "Terminal.");
    terminal_writestring("\n");
    terminal_writestring("Welcome to KatelynOS!\n");
}
