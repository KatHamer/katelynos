# Test script for KatelynOS
# By Kat Hamer

img="images/katos.iso"
serial="stdio"

echo "Running QEMU [katos.iso]..."
qemu-system-i386 -cdrom $img -no-reboot -serial $serial
