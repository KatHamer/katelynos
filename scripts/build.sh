# Build script for KatelynOS
# By Kat Hamer

iso="iso"
bld="bld"
src="."
dsk="images"

mkdir -p images/

echo "Compiling bootloader stub [boot.asm]..."
nasm -felf32 $src/boot.asm -o bld/boot.o
echo "Compiling kernel [kernel.c]..."
i686-elf-gcc -c $src/kernel.c -o $bld/kernel.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
echo "Linking kernel..."
i686-elf-gcc -T linker.ld -o $iso/boot/kernel -ffreestanding -O2 -nostdlib $bld/boot.o $bld/kernel.o -lgcc
echo "Making GRUB ISO... [katos.iso]"
grub-mkrescue -o $dsk/katos.iso $iso
echo "Done!"
